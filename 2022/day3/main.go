package main

import (
	"fmt"
	"os"
	"strings"
)

var alphabet map[rune]int

func main() {
	alphabet = GetAlphabetMap()

	// Read file, split into lines
	b, err := os.ReadFile("./input.txt")
	if err != nil {
		fmt.Printf("Unable to open input.txt: %s\n", err.Error())
		os.Exit(1)
	}
	content := string(b)
	content = strings.TrimRight(content, "\n")
	lines := strings.Split(content, "\n")

	// Set up counters for total score and group score
	score := 0
	groupScore := 0

	// Make a []string to hold 3 lines
	lineGroup := make([]string, 0)
	
	for i,v := range lines {
		// Verify each line is even
		if len(v) % 2 != 0 {
			fmt.Printf("Line %d is not divisible by 2 evenly\n", i + 1)
			os.Exit(1)
		}
		
		// Append the line to the lineGroup [] and check if length is 3
		lineGroup = append(lineGroup, v)
		if len(lineGroup) == 3 {
			// Pass into ProcessLineGroup() to get the total group score
			// and sum of individual line scores and add them to the overall scores
			lineVal, groupVal := ProcessLineGroup(lineGroup)
			score += lineVal
			groupScore += groupVal

			// Reset the linegroup [] for next 3 lines
			lineGroup = nil
		}
	}

	fmt.Printf("Line Score: %d, Group score: %d\n", score, groupScore)
}

func ProcessLineGroup(group []string) (int, int) {
	// Set up return values
	lineScore := 0
	groupScore := 0
	
	line1 := group[0]
	line2 := group[1]
	line3 := group[2]

	// Grab the total score for each individual elf
	lineScore = lineScore + GetLineScore(line1) + GetLineScore(line2) + GetLineScore(line3)

	for _, v := range line1 {
		if strings.ContainsRune(line2, v) && strings.ContainsRune(line3, v) {
			groupScore += alphabet[v]
			return lineScore, groupScore
		}
	}

	// Made is past token checking, something went wrong
	return 0, 0
}

func GetLineScore(line string) int {
	// Split the line in half
	half := len(line) / 2
	first := line[half:]
	second := line[:half]

	// Loop over every item in first and second
	for _, c := range first {
		if strings.ContainsRune(second, c) {
			return alphabet[c]
		}
	}
	return 0
}

func GetAlphabetMap() map[rune]int {
	a := make(map[rune]int)
	counter := 1
	for _, v := range "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" {
		a[v] = counter
		counter++
	}
	return a
}