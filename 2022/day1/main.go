package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	// Read input.txt and split it into lines
	rawBytes, err := os.ReadFile("./input.txt")
	if err != nil {
		fmt.Printf("Unable to open input.txt: %s\n", err.Error())
		os.Exit(1)
	}

	content := string(rawBytes)
	content = strings.TrimRight(content, "\n")
	lines := strings.Split(content, "\n")

	// Set up []int to hold each elf's calories
	elves := make([]int, 0)

	// Set up counter to hold total amount of calories
	calories := 0

	for _, v := range lines {
		
		// Trim any space
		v = strings.TrimSpace(v)

		// if line is empty either there is not current elf or between elves
		if len(v) == 0 {

			// Check if started counting an elf's calories
			if calories > 0 {

				// calories is not 0, append the calories to the elves [] and
				// reset the calorie counter
				elves = append(elves, calories)
				calories = 0
			}
			continue // break out of current loop cycle
		}

		// Convert the line value to int and add it to calories counter
		intval, err := strconv.ParseInt(v, 0, 64)
		if err != nil {
			fmt.Printf("Failed to convert %s to int64: %s", v, err.Error())
			os.Exit(1)
		}
		calories += int(intval)
	}

	// Sort the elves [] and grab the last 3 elements and print
	sort.Ints(elves)
	
	top3 := elves[len(elves) - 3:]

	counter := 1

	for i := len(top3) - 1; i >= 0; i-- {
		fmt.Printf("%d: %d\n", counter, top3[i])
		counter++
	}
}
