package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

/* Answer1: FCVRLMVQP    | Answer2: RWLWGJGFD

    [M]             [Z]     [V]
    [Z]     [P]     [L]     [Z] [J]
[S] [D]     [W]     [W]     [H] [Q]
[P] [V] [N] [D]     [P]     [C] [V]
[H] [B] [J] [V] [B] [M]     [N] [P]
[V] [F] [L] [Z] [C] [S] [P] [S] [G]
[F] [J] [M] [G] [R] [R] [H] [R] [L]
[G] [G] [G] [N] [V] [V] [T] [Q] [F]
 1   2   3   4   5   6   7   8   9
*/

/*
    [D]
[N] [C]
[Z] [M] [P]
 1   2   3
*/

func main() {
	d, _ := os.ReadFile("./input.txt")
	lines := strings.Split(string(d), "\n")
	PartOne(lines)
	PartTwo(lines)
}

func PartOne(lines []string) {
	crates := GetCrates()
	for _, v := range lines {
		fields := strings.Fields(v)
		// fields[1] = num to move, fields[3] = src col, fields[5] = dest col

		num, err := strconv.Atoi(fields[1])
		if err != nil {
			fmt.Printf("%s\n", err.Error())
			os.Exit(1)
		}

		srcSlice := crates[fields[3]]
		destSlice := crates[fields[5]]

		for i := num; i > 0; i-- {
			elem := srcSlice[len(srcSlice)-1]

			destSlice = append(destSlice, elem)
			srcSlice = srcSlice[:len(srcSlice)-1]
			crates[fields[3]] = srcSlice
			crates[fields[5]] = destSlice
		}
	}

	fmt.Printf("%s: %s\n", "1", crates["1"])
	fmt.Printf("%s: %s\n", "2", crates["2"])
	fmt.Printf("%s: %s\n", "3", crates["3"])
	fmt.Printf("%s: %s\n", "4", crates["4"])
	fmt.Printf("%s: %s\n", "5", crates["5"])
	fmt.Printf("%s: %s\n", "6", crates["6"])
	fmt.Printf("%s: %s\n", "7", crates["7"])
	fmt.Printf("%s: %s\n", "8", crates["8"])
	fmt.Printf("%s: %s\n", "9", crates["9"])
}

func PartTwo(lines []string) {
	crates := GetCrates()
	for _, v := range lines {

		fields := strings.Fields(v)
		// fields[1] = num to move, fields[3] = src col, fields[5] = dest col

		num, err := strconv.Atoi(fields[1])
		if err != nil {
			fmt.Printf("%s\n", err.Error())
			os.Exit(1)
		}

		srcSlice := crates[fields[3]]
		destSlice := crates[fields[5]]

		fmt.Printf("Moving %d elements from col %s to col %s\n", num, fields[3], fields[5])
		fmt.Printf("    starting length of srcSlice: %d\n", len(srcSlice))

		elems := srcSlice[len(srcSlice)-num:]
		srcSlice = srcSlice[:len(srcSlice)-num]
		fmt.Printf("    changed length of srcSlice: %d\n", len(srcSlice))
		fmt.Printf("  moving %s\n", elems)

		destSlice = append(destSlice, elems...)

		crates[fields[3]] = srcSlice
		crates[fields[5]] = destSlice
	}

	fmt.Printf("%s: %s\n", "1", crates["1"])
	fmt.Printf("%s: %s\n", "2", crates["2"])
	fmt.Printf("%s: %s\n", "3", crates["3"])
	fmt.Printf("%s: %s\n", "4", crates["4"])
	fmt.Printf("%s: %s\n", "5", crates["5"])
	fmt.Printf("%s: %s\n", "6", crates["6"])
	fmt.Printf("%s: %s\n", "7", crates["7"])
	fmt.Printf("%s: %s\n", "8", crates["8"])
	fmt.Printf("%s: %s\n", "9", crates["9"])
}

func GetCrates() map[string][]string {
	crates := make(map[string][]string)
	crates["1"] = []string{"G", "F", "V", "H", "P", "S"}
	crates["2"] = []string{"G", "J", "F", "B", "V", "D", "Z", "M"}
	crates["3"] = []string{"G", "M", "L", "J", "N"}
	crates["4"] = []string{"N", "G", "Z", "V", "D", "W", "P"}
	crates["5"] = []string{"V", "R", "C", "B"}
	crates["6"] = []string{"V", "R", "S", "M", "P", "W", "L", "Z"}
	crates["7"] = []string{"T", "H", "P"}
	crates["8"] = []string{"Q", "R", "S", "N", "C", "H", "Z", "V"}
	crates["9"] = []string{"F", "L", "G", "P", "V", "Q", "J"}
	return crates
}

func GetCratesExample() map[string][]string {
	crates := make(map[string][]string)
	crates["1"] = []string{"Z", "N"}
	crates["2"] = []string{"M", "C", "D"}
	crates["3"] = []string{"P"}
	return crates
}
