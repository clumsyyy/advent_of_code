package main

import (
	"fmt"
	"os"
	"strings"
)

var (
	WIN      = 6
	DRAW     = 3
	LOSS     = 0
	
	ROCK     = 1
	PAPER    = 2
	SCISSORS = 3
)

func main() {
	// Open and read file, split into []string of lines
	f, err := os.ReadFile("./input.txt")
	if err != nil {
		fmt.Printf("Unable to open file: %s\n", err.Error())
		os.Exit(1)
	}

	content := string(f)
	content = strings.TrimRight(content, "\n")
	lines := strings.Split(content, "\n")

	// Set up a counter to collect sore
	score := 0

	for i, v := range lines {
		v = strings.TrimRight(v, "\n")
		score += getScore(v, i)
	}

	fmt.Printf("Score: %d\n", score)
}

func getScore(line string, num int) int {
	/*	ELF				Card to play
		A = rock		X = lose
		B = paper		Y = draw
		C = scissors	Z = win
	*/

	// Check all possible combos
	switch line {
	case "A X":
		return SCISSORS + LOSS
	
	case "A Y":
		return ROCK + DRAW
	
	case "A Z":
		return PAPER + WIN
	
	case "B X":
		return ROCK + LOSS
	
	case "B Y":
		return PAPER + DRAW
	
	case "B Z":
		return SCISSORS + WIN
	
	case "C X":
		return PAPER + LOSS
	
	case "C Y":
		return SCISSORS + DRAW
	
	case "C Z":
		return WIN + ROCK
	
	default:
		fmt.Printf("Hit unknown line combo at line %d: |%s|\n", num, line)
		return 0
	}
}
