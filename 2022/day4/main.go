package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type ElfRange struct {
	Start int
	End   int
}

func main() {
	raw, err := os.ReadFile("./input.txt")
	if err != nil {
		log.Fatalf("Failed to open input.txt")
	}
	lines := strings.Split(strings.TrimRight(string(raw), "\n"), "\n")
	PartOne(lines)
	PartTwo(lines)
}

func PartOne(lines []string) {
	counter := 0
	// Loop over each line
	for _, v := range lines {
		pair := strings.Split(v, ",")

		elfRangeStr1 := strings.Split(pair[0], "-")
		elfRangeStr2 := strings.Split(pair[1], "-")

		elf1Start, _ := strconv.Atoi(elfRangeStr1[0])
		elf1End, _ := strconv.Atoi(elfRangeStr1[1])

		elf2Start, _ := strconv.Atoi(elfRangeStr2[0])
		elf2End, _ := strconv.Atoi(elfRangeStr2[1])

		range1 := make([]int, 0)
		range2 := make([]int, 0)

		for i := elf1Start; i <= elf1End; i++ {
			range1 = append(range1, i)
		}

		for i := elf2Start; i <= elf2End; i++ {
			range2 = append(range2, i)
		}

		if elf1Start >= elf2Start && elf1End <= elf2End {
			// fmt.Printf("%d: %s\t\tElf 1 is in range of elf2\n", i, v)
			counter++
		} else if elf2Start >= elf1Start && elf2End <= elf1End {
			// fmt.Printf("%d: %s\t\tElf 2 is in range of elf1\n", i, v)
			counter++
		}
	}
	fmt.Printf("Part 1 counter: %d\n", counter)
}

func PartTwo(lines []string) {
	counter := 0

	for _, v := range lines {
		pair := strings.Split(v, ",")
		elfRangeStr1 := strings.Split(pair[0], "-")
		elfRangeStr2 := strings.Split(pair[1], "-")

		elf1Start, _ := strconv.Atoi(elfRangeStr1[0])
		elf1End, _ := strconv.Atoi(elfRangeStr1[1])

		elf2Start, _ := strconv.Atoi(elfRangeStr2[0])
		elf2End, _ := strconv.Atoi(elfRangeStr2[1])

		if elf1Start >= elf2Start && elf1Start <= elf2End {
			counter++
			continue
		}

		if elf2Start >= elf1Start && elf2Start <= elf1End {
			counter++
			continue
		}
	}
	fmt.Printf("Part 2 counter: %d\n", counter)
}
